export default {
  name: "FieldBox",
  props: {
    lvl1h: {
      type: Number,
      required: true
    },
    lvl1w: {
      type: Number,
      required: true
    },
    boxLength: {
      type: Number,
      required: false
    }
  },
  data() {
    return {
      fieldMatrix: [],
      grams: [],
      picked_n: -1,
      playLock: false,
    }
  },
  methods: {
    tileValue(row, col) {
      if (this.fieldMatrix[row] === undefined || this.fieldMatrix[row][col] === undefined) {
        return -1
      } else {
        return this.fieldMatrix[row][col]
      }
    },
    placingDigit() {
      for (let i = 0; i < this.lvl1h; i++) {
        for (let j = 0; j < this.lvl1w; j++) {
          if (this.fieldMatrix[i][j] === 9) {
            for (let ii = -1; ii <= 1; ii++) {
              for (let jj = -1; jj <= 1; jj++) {
                if (ii !== 0 || jj !== 0) {
                  if (this.tileValue(i + ii, j + jj) !== 9 && this.tileValue(i + ii, j + jj) !== -1) {
                    this.fieldMatrix[i + ii][j + jj]++
                  }
                }
              }
            }
          }
          if (this.fieldMatrix[i][j] === 30) {
          }
        }
      }
    },
    runNum() {

    },
    setup() {
      let h = 0
      this.fieldMatrix = []
      // creating on array
      for (let i = 0; i < this.lvl1h; i++) {
        this.fieldMatrix[i] = []
        for (let j = 0; j < this.lvl1w; j++) {
          this.fieldMatrix[i].push(0)
        }
      }
      for (let i = 0; i < this.lvl1h; i++) {
        for (let j = 0; j < this.lvl1w; j++) {
          this.grams.push({
            x: i,
            y: j,
            i: h
          })
          h++
        }
      }
    }
  }
}
