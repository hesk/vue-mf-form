//import PincodeInput from "./components/codepin/Component.vue"
import PricePlan from "../components/priceplan/Component.vue";
export default {
    name: "App",
    components: {
        PricePlan,
    },
    data: () => ({
        code: "",
        messsage: "",
        iserror: false,
    }),
    methods: {
        triError() {
            let t = this.$refs["fillingpincode"];
            this.iserror = !this.iserror;
            t.setError(this.iserror);
        },
        reset() {
            let t = this.$refs["fillingpincode"];
            t.reset();
            //      this.code = ""
        },
    }
};
//# sourceMappingURL=App.vue.js.map