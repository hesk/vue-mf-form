# vue-minefield
The great mine field component for Vue.js applications.

![vue-minefield](https://user-images.githubusercontent.com/629338/110722716-ba9ee400-824d-11eb-93ba-3ea3933d099b.png)

Demo on [GitHub Pages](https://seokky.github.io/vue-minefield/)

## Features
- custom configurations
- locked, unlock, load data, clear reset, disabled, undisabled
- full scss style framework

## Attention!

Styles that component have are written just for demo. But that styles are override-friendly, so you can write any styles you want.

## Usage

```
  npm i --save vue-minefield
```
or with yarn
```
  yarn add vue-minefield
```

Then in any component:

```
import MineField from 'vue-minefield';
// The name can be different depending on your desire
```

```
import {MineField} from "vue-minefield"


<div class="input-wrapper">
  <MineField
        ref="pickmatrix"
             :lvl1w="8"
             :lvl1h="5"
             :box-length="40 + 10"
             :max-picked="max_pick"
             @update_list="setlist"
  />
</div>
```

**Attention**: you should use .. in order to rule  ...

```
<style lang="scss">
@import "@node_modules/vue-minefield/src/presents/base";

.box_nfx {
  margin: 1px;
  border: 1px solid #222;
  @include btn3dbase;
  @include btn3dGreen;
  // any styles you want for each cell
  

  span {
    font-size: 14px;
    color: #ffffff;
    align-self: center;
    justify-self: center;
    width: 100%;
  }


  &.highlight {
    @include claw_machine_button_style_ex1(#73ff4a);
    @include activeButton;

    span {
      color: #c61616;
    }
  }

}

<style>
```

## Props

- **lvl1w** (number of columns)
  - type: Number
  - default: 0

- **lvl1h** (number of rows)
  - type: Number
  - default: 0

- **box-length** (the size of the box)
  - type: Number
  - default: 0

- **max-picked** (preview character on typing)
  - type: Boolean
  - default: true

- **update_list** (event during updating the list items)
  - type: Event

- **exceed_max_picked** (event that exceed the max picked counts)
  - type: Event
  
- **error_locked** (event error from locked)
  - type: Event

- **locked** (event locked)
  - type: Event
  
## ToDo

    Note: in Minesweeper mode, you need to click at least 7 times with 1 bomb, 5 times with 3 bombs, 3 times with 5
        bombs or once with 24 bombs in order to receive wager.
    
         The player can login the bigbang game platform and make deposit of the token BBGC, HT, ZB, OKB, ETH, GTS and the
            supported tokens. The player can obtain the tokens from the exchange platforms. HOTBIT.io, FCOIN.com, EXX.com,
            and ZBG.com.
